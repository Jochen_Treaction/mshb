<?php


namespace App\Services;


use Exception;
use Psr\Log\LoggerInterface;

class ToolServices
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $encryptedStr
     * @return array|null
     * @author Pradeep
     */
    public function decrypt(string $encryptedStr):?array
    {
        try {
            if (empty($encryptedStr)) {
                throw new Exception('Invalid Encrypted string');
            }
            $str = base64_decode($encryptedStr);
            if (empty($str)) {
                throw new Exception('base64_decode failed');
            }
            return json_decode($str, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException | Exception $e) {
            var_dump($e->getMessage());
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function convertToSnakeCase(string $string):?string
    {

    }
}