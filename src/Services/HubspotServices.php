<?php


namespace App\Services;

use App\Exceptions\HubspotException;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class HubspotServices
 * @package App\Services
 * @author Pradeep
 */
class HubspotServices
{
    /**
     * @var HttpClientInterface
     * @author Pradeep
     */
    private $http_client;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var string
     * @author Pradeep
     */
    private $api_key;

    /**
     * HTTP SUCCESS AND ERROR CODE
     * FOR VALIDATING THE RESPONSE FROM HUBSPOT API
     */
    public CONST HTTP_SUCCESS_CODE = [200, 204];
    public CONST HTTP_ERROR_CODE = [400, 401, 404, 500];

    public function __construct(LoggerInterface $logger, ToolServices $toolServices)
    {
        $this->http_client = new CurlHttpClient();
        $this->toolServices = $toolServices;
        $this->logger = $logger;
    }

    /**
     * @param string $api_key
     * @return bool
     * @author Pradeep
     */
    public function setApiKey(string $api_key): bool
    {
        if (empty($api_key)) {
            return false;
        }
        $this->api_key = $api_key;
        return true;
    }

    /**
     * @param array $properties
     * @return int
     * @author Pradeep
     */
    public function createContact(array $properties): int
    {
        $contactId = 0;
        $apikey = $this->getApiKey();
        if(!isset($properties['email'])) {
            return $contactId;
        }
        $contactIdFromHb = $this->isContactExists($properties[ 'email' ]);
        if ( $contactIdFromHb > 0) {
            $end_point = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$contactIdFromHb.'/profile?hapikey='.$apikey;
            $this->logger->info('cc_update_url '.$end_point);
            $operation = 'update';
        } else {
            $end_point = 'https://api.hubapi.com/contacts/v1/contact/?hapikey=' . $apikey;
            $operation='insert';
        }
        $contact_properties = $this->generateContactProperties($properties);
        $this->logger->info('contactProperty ' . json_encode($contact_properties, JSON_THROW_ON_ERROR));
        $result = $this->sendHttpRequest('POST', $end_point, $contact_properties,
            ['Content-Type: application/json']);
        if(($operation === 'insert') && (!empty($result) && isset($result[ 'vid' ]) && $result[ 'vid' ] > 0)) {
            $contactId = $result[ 'vid' ];
        }
        if($operation === 'update') {
            $contactId = $contactIdFromHb;
        }

        return $contactId;
    }

    /**
     * @return string
     * @author Pradeep
     */
    private function getApiKey(): string
    {
        if (!isset($this->api_key) || empty($this->api_key)) {
            return '';
        }
        return $this->api_key;
    }

    /**
     * @param string $email
     * @return int
     * @author Pradeep
     */
    private function isContactExists(string $email): int
    {
        $contact_id = 0;
        try {
            if (empty($email)) {
                throw new RuntimeException('Invalid Email Address');
            }
            $apikey = $this->getApiKey();
            $end_point = 'https://api.hubapi.com/contacts/v1/contact/email/' . $email . '/profile?hapikey=' . $apikey;
            $result = $this->sendHttpRequest('GET', $end_point, [], []);
            if (!empty($result) && isset($result[ 'vid' ]) && $result[ 'vid' ] > 0) {
                $contact_id = $result[ 'vid' ];
            }
            return $contact_id;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contact_id;
        }

        return $contact_id;
    }

    /**
     * @param string $method
     * @param string $end_point
     * @param array $body
     * @param array $headers
     * @return array
     * @author Pradeep
     */
    private function sendHttpRequest(string $method, string $end_point, array $body, array $headers): array
    {
        $resp_as_array = [];
        try {
            if (empty($end_point) || empty($method)) {
                throw new RuntimeException('Invalid Endpoint or Method provided');
            }
            $response = $this->http_client->request($method, $end_point, ['headers' => $headers, 'json' => $body]);
            $this->logger->info('statusCode '.$response->getStatusCode());
            if(!in_array($response->getStatusCode(), self::HTTP_SUCCESS_CODE)) {
                throw new RuntimeException('Failed to send request ' . json_encode($response->getContent(),
                        JSON_THROW_ON_ERROR));
            }
            $resp_as_array = $response->toArray();

        } catch (ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $resp_as_array;
    }

    /**
     * @param array $properties
     * @return array
     * @author Pradeep
     */
    private function generateContactProperties(array $properties): array
    {
        $contact_prop = [];
        $result = [];

        try {
            foreach ($properties as $key => $value) {
                $contactPropertyFromHB = $this->getContactProperty($key);
                $this->logger->info('ContactProperty ' . json_encode($contactPropertyFromHB, JSON_THROW_ON_ERROR));
                if (empty($key) || empty($contactPropertyFromHB)) {
                    throw new RuntimeException('ContactProperty ' . json_encode($contactPropertyFromHB, JSON_THROW_ON_ERROR));
                    continue;
                }
                $result[] = [
                    'property' => $key,
                    'value' => $value,
                ];
            }// As per the Hubspot API
            if (!empty($result)) {
                $contact_prop[ 'properties' ] = $result;
            }
            return $contact_prop;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
            return $contact_prop;
        }
    }

    /**
     * @param array $properties
     * @return int
     * @author Pradeep
     */
    public function createCompany(array $properties): int
    {
        $company_id = 0;
        $apikey = $this->getApiKey();
        $end_point = 'https://api.hubapi.com/companies/v2/companies?hapikey=' . $apikey;
        $company_properties = $this->generateCompanyProperties($properties);
        $result = $this->sendHttpRequest('POST', $end_point, $company_properties, ['Content-Type: application/json']);
        if (!empty($result) && isset($result[ 'companyId' ]) && $result[ 'companyId' ] > 0) {
            $company_id = $result[ 'companyId' ];
        }
        return $company_id;
    }

    /**
     * @param array $properties
     * @return array
     * @author Pradeep
     */
    public function generateCompanyProperties(array $properties): array
    {
        $company_prop = [];
        $result = [];

        foreach ($properties as $key => $value) {
            if (empty($key)) {
                continue;
            }
            $result[] = [
                'name' => $key,
                'value' => $value,
            ];
        }
        // As per the Hubspot Company API
        if (!empty($result)) {
            $company_prop[ 'properties' ] = $result;
        }

        return $company_prop;
    }

    /**
     * @param array $properties
     * @return int
     * @author Pradeep
     */
    public function createDeal(array $properties): int
    {
        $deal_id = 0;
        $apikey = $this->getApiKey();
        $end_point = 'https://api.hubapi.com/deals/v1/deal?hapikey=' . $apikey;
        $deal_properties = $this->generateDealProperties($properties);
        $result = $this->sendHttpRequest('POST', $end_point, $deal_properties, ['Content-Type: application/json']);
        if (!empty($result) && isset($result[ 'dealId' ]) && $result[ 'dealId' ] > 0) {
            $deal_id = $result[ 'dealId' ];
        }
        return $deal_id;
    }

    /**
     * @param array $properties
     * @return array
     * @author Pradeep
     */
    private function generateDealProperties(array $properties): array
    {
        $deal_prop = [];
        $result = [];

        foreach ($properties as $key => $value) {
            if (empty($key) || ($key === 'contact_id') || ($key === 'company_id')) {
                continue;
            }
            $result[] = [
                'name' => $key,
                'value' => $value,
            ];
        }

        // As per the Hubspot API
        if (!empty($result)) {
            $deal_prop[ 'associations' ] = [
                'associatedCompanyIds' => $properties[ 'company_id' ],
                'associatedVids' => $properties[ 'contact_id' ],
            ];
            $deal_prop[ 'properties' ] = $result;
        }

        return $deal_prop;
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getContactProperties(): array
    {
        $api_key = $this->getApiKey();
        $url = 'https://api.hubapi.com/properties/v1/contacts/properties?hapikey=' . $api_key;
        return $this->sendHttpRequest('GET', $url, [], []);
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getDealProperties(): array
    {
        $api_key = $this->getApiKey();
        $url = 'https://api.hubapi.com/properties/v1/deals/properties/?hapikey=' . $api_key;
        return $this->sendHttpRequest('GET', $url, [], []);
    }

    /**
     * @param string $encoded_string
     * @return array
     * @author Pradeep
     */
    public function validateRequest(string $encoded_string): array
    {
        $req_arr = [];
        try {
            if (empty($encoded_string)) {
                throw new RuntimeException('empty encoded string ');
            }
            $req_json = base64_decode($encoded_string);
            $req_arr = json_decode($req_json, true, 512, JSON_THROW_ON_ERROR);
            return $req_arr;
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
            return $req_arr;
        }
    }

    /**
     * @param string $domain
     * @return int
     * @author Pradeep
     */
    public function searchCompanyByDomain(string $domain): int
    {
        $company_id = 0;
        if (empty($domain) || $this->http_client === null) {
            return $company_id;
        }
        $apikey = $this->getApiKey();
        $url = 'https://api.hubapi.com/companies/v2/domains/' . $domain . '/companies?hapikey=' . $apikey;
        $hb_search_company_properties = [
            'limit' => 1,
            'requestOptions' => [
                'properties' => [
                    'domain',
                    'name',
                ],
            ],
            'offset' => [
                'isPrimary' => true,
                'companyId' => 0,
            ],
        ];
        $resp = $this->sendHttpRequest('POST', $url, $hb_search_company_properties, ['Content-Type: application/json']);
        if (!empty($resp) && !empty($resp[ 'results' ][ 0 ][ 'companyId' ])) {
            $company_id = $resp[ 'results' ][ 0 ][ 'companyId' ];
        }
        return $company_id;
    }

    /**
     * @param array $property
     * @return array
     * @author Pradeep
     * @internal  Creates a new Contact Property.
     */
    public function createContactProperty(array $property):array {
        $contactProperty = [];
        $apikey = $this->getApiKey();
        $end_point = 'https://api.hubapi.com/properties/v1/contacts/properties?hapikey=' . $apikey;
        try {
            if(empty($apikey)) {
                throw new HubspotException('Invalid Hubspot APIKey');
            }
            if (empty($property['name']) || empty($property['label'])) {
                throw new HubspotException('Invalid Parameters');
            }

            $payload = [
                // Hubspot Name has to be in LowerCase
                'name' => strtolower($property['name']),
                "label"=> $property['label'],
                "description"=> $property['description'] ?? '',
                "groupName"=> $property['groupName'] ?? "contactinformation",
                "type"=> $property['type'] ?? "string",
                "fieldType"=>$property['fieldType'] ?? "text",
                "formField"=>$property['formField'] ?? true,
                "displayOrder"=>$property['displayOrder'] ?? 6,
                "options"=>$property['options'] ?? []
            ];
            $this->logger->info('payload info '.json_encode($payload));
            $result = $this->sendHttpRequest('POST', $end_point, $payload,[]);
            $this->logger->info(json_encode($result));
            if (!empty($result) && isset($result[ 'name' ])) {
                $contactProperty = $result;
            }
            return $contactProperty;
        } catch (HubspotException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $contactProperty;
        }
    }


    /**
     * @param string $propertyName
     * @return array|null
     * @throws \JsonException
     * @author Pradeep
     */
    public function getContactProperty(string $propertyName):?array
    {
        $apikey = $this->getApiKey();

        try {
            if (empty($propertyName) || empty($apikey)) {
                throw new HubspotException('Invalid APIKey or Property Name');
            }
            $lowerCaseName = strtolower($propertyName);
            $end_point = 'https://api.hubapi.com/properties/v1/contacts/properties/named/' . $lowerCaseName . '?hapikey=' . $apikey;
            return $this->sendHttpRequest('GET', $end_point, [], []);
        } catch (HubspotException | JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }
}
