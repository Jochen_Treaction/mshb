<?php


namespace App\Controller;

use App\Exceptions\HubspotException;
use App\Services\HubspotServices;
use App\Services\ToolServices;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HubspotController
 * @package App\Controller
 * @author Pradeep
 */
class HubspotController extends AbstractController
{
    /**
     * @Route( "/contact/property/create", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     */
    public function createContactProperty(
        Request $request,
        HubspotServices $hubspotServices,
        ToolServices $toolServices,
        LoggerInterface $logger
    ): JsonResponse {
        $content = $toolServices->decrypt($request->getcontent());
        $result = [];
        try {
            if (empty($content) || empty($content[ 'property' ]) || empty($content['property']['name']) ||
                !$hubspotServices->setApiKey($content[ 'apikey' ])) {
                throw new HubspotException('Missing Property or APIKey');
            }
            // CHECK IF PROPERTY EXITS.
            $result = $hubspotServices->getContactProperty($content['property']['name']);
            if(empty($result)) {
                // CREATE A NEW PROPERTY.
                $result = $hubspotServices->createContactProperty($content[ 'property' ]);
            }
            if (empty($result)) {
                throw new HubspotException('Failed to create Contact Parameter ' . json_encode($content[ 'property' ]));
            }
            return $this->json([
                'content' => $result,
                'status' => true,
                'message' => 'Successfully create Contact Parameter.',
            ]);
        } catch (HubspotException | JsonException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json([
                'content' => $result,
                'status' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Route No. 1,
     * @Route( "/contact/properties/get", name="hb.allproperties", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function getContactProperties(
        Request $request,
        HubspotServices $hubspotServices,
        ToolServices $toolServices,
        LoggerInterface $logger
    ): Response {
        $properties = [];
        $content = $toolServices->decrypt($request->getContent());
        try {
            if (empty($content) || !$hubspotServices->setApiKey($content['apikey'])) {
                throw new HubspotException('Invalid APIKey');
            }
            $properties = $hubspotServices->getContactProperties();
            if (empty($properties)) {
                throw new HubspotException('Failed to get Contact Parameters.');
            }
            return $this->json([
                'content' => $properties,
                'status' => true,
                'message' => 'Successfully fetched Contact Properties.',
            ]);
        } catch (HubspotException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json([
                'content' => $properties,
                'status' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Route No. 2,
     * @Route( "/contact/create", name="hb.properties", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function createContact(Request $request, HubspotServices $hubspotServices, LoggerInterface $logger): Response
    {
        $content = $request->getContent();
        $response = new Response();
        $req_arr = $hubspotServices->validateRequest($content);
        $logger->info('Request Array ' . json_encode($req_arr, JSON_THROW_ON_ERROR));
        try {
            if(empty($req_arr) || empty($req_arr[ 'apikey' ]) || empty($req_arr[ 'contact' ])) {
                throw new RuntimeException('Invalid APIKey or Contact data provided');
            }
            $api_key = $req_arr[ 'apikey' ];
            $contact_properties = $req_arr[ 'contact' ];
            if (empty($api_key) || !$hubspotServices->setApiKey($api_key)) {
                return $response->setContent(json_encode(['error' => 'Invalid APIKey'], JSON_THROW_ON_ERROR));
            }
            $id = $hubspotServices->createContact($contact_properties);
            return $response->setContent(json_encode(['id' => $id], JSON_THROW_ON_ERROR));
        } catch (JsonException | Exception $e) {
            print_r($e->getMessage());
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $response->setContent('');
        }
    }

    /**
     * Route No. 3,
     * @Route( "/company/create", name="hb.create_company", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     *
     * @return Response
     * @author Pradeep
     */
    public function createCompany(Request $request, HubspotServices $hubspotServices): Response
    {
        $content = $request->getContent();
        $req_arr = $hubspotServices->validateRequest($content);
        $api_key = $req_arr[ 'apikey' ];
        $response = new Response();
        $company_properties = $req_arr[ 'company' ];
        if (empty($api_key) || !$hubspotServices->setApiKey($api_key)) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }
        if (empty($company_properties)) {
            return $response->setContent(json_encode(['error' => 'Invalid Company properties']));
        }
        $search_company_id = $hubspotServices->searchCompanyByDomain($company_properties[ 'name' ]);
        if ($search_company_id <= 0) {
            $id = $hubspotServices->createCompany($company_properties);
        } else {
            $id = $search_company_id;
        }

        return $response->setContent(json_encode(['id' => $id]));
    }

    /**
     * Route No. 4,
     * @Route( "/deal/create", name="hb.create_deal", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @return Response
     * @author Pradeep
     */
    public function createDeal(Request $request, HubspotServices $hubspotServices): Response
    {
        $content = $request->getContent();
        $req_arr = $hubspotServices->validateRequest($content);
        $api_key = $req_arr[ 'apikey' ];
        $response = new Response();
        $deal_properties = $req_arr[ 'deal' ];
        if (empty($api_key) || !$hubspotServices->setApiKey($api_key)) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }
        if (empty($deal_properties)) {
            return $response->setContent(json_encode(['error' => 'Invalid Deal properties']));
        }
        $id = $hubspotServices->createDeal($deal_properties);
        return $response->setContent(json_encode(['id' => $id]));
    }


    /**
     * Route No. 5,
     * @Route( "/integratelead", name="hb.integratelead", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @return Response
     * @author Pradeep
     */
    public function integrateLeadToHubspot(Request $request, HubspotServices $hubspotServices): Response
    {
        $response = new Response();
        $api_key = $request->get('apikey');
        $prop_contact = $request->get('contact');

        if (empty($api_key) || !$hubspotServices->setApiKey($api_key)) {
            return $response->setContent(json_encode(['error' => 'Invalid APIKey']));
        }
        return $hubspotServices->createContact(json_decode($prop_contact, true));
    }

    /**
     * Route No. 2,
     * @Route( "/apikey/check", name="hb.integratelead", methods={"POST"})
     * @param Request $request
     * @param HubspotServices $hubspotServices
     * @return Response
     * @author Pradeep
     */
    public function isApiValid(Request $request, HubspotServices $hubspotServices): Response
    {
        $status = false;
        $response = new Response();
        $api_key = $request->get('apikey');
        if (empty($api_key) || !isset($api_key) || !$hubspotServices->setApiKey($api_key)) {
            return $response->setContent(json_encode($status));
        }
        $prop_contact = $hubspotServices->getContactProperties();
        if (!empty($prop_contact)) {
            $status = true;
        }

        return $response->setContent(json_encode($status));
    }

    /**
     * @Route( "/check/alive", name="checkAlive", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAlive(Request $request):JsonResponse
    {
        return $this->json([
            'success' => true,
            'msg' => 'I am Alive',
            'yourIp' => $request->getClientIp(),
            'locale' => $request->getLocale(),
            'protocolVersion' => $request->getProtocolVersion(),
        ]);
    }
}
